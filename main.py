import cv2

def analyser():
    cascadef_ilter_path = 'C:/PYTHON/Video_face_id/filter/haarcascade_frontalface_default.xml'
    clf = cv2.CascadeClassifier(cascadef_ilter_path)
    camera = cv2.VideoCapture('name of video')
    while True:
        _, frame = camera.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = clf.detectMultiScale(
            gray,
            scaleFactor=1.1,
            minNeighbors=10,
            minSize=(30, 30),
            flags=cv2.CASCADE_SCALE_IMAGE
        )
        for (x, y, x_width, y_height) in faces:
            cv2.rectangle(frame, (x, y), (x + x_width, y + y_height), (0, 0, 255), 3)
        cv2.imshow('Faces', frame)
        if cv2.waitKey(1) == ord('+'):
            break
    camera.release()
    cv2.destroyAllWindows()

def main():
    analyser()

if __name__ == '__main__':
    main()