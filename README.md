# Fase recognizer

Video fase recognizer

## Installation

You need install python 3.9:
```
pip install python
```
## Virtual environment

In terminal in project folder you should activate virtual environment:

```
venv/Scripts/activate
```

## Add important libraries

Needed libraries:

```
numpy==1.23.2
opencv-python==4.6.0.66

```

## Getting started

Start app:
```
main.py
```
## Technologies used
 
- Python


## Description

The app allows recognize all faces in the video and then saving pictures of each person to the database.

All detailed information you can find in the code above.

## Author

This app was done by Dmitry Tsunaev.

- [ ] [LinkedIn](http://linkedin.com/in/dmitry-tsunaev-530006aa)
